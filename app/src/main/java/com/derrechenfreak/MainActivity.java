package com.example.emiram.derrechenfreak;

import android.app.Dialog;

import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RadioButton;

public class MainActivity extends AppCompatActivity {

    private ImageView logo;
    private Button schwierigkeitButton;
    private Button anleitungButton;
    private Button operationButton;
    private ImageButton goButton;
    private Button okDiffButton;
    private Button okOpeButton;
    private Button okInstrucButton;
    private Dialog diffPop;
    private Dialog opePop;
    private Dialog instrucPop;
    private RadioButton leichtButton;
    private RadioButton mittelButton;
    private RadioButton schwerButton;
    private RadioButton addButton;
    private RadioButton subButton;
    private RadioButton multButton;
    private boolean leicht = true;
    private boolean mittel = false;
    private boolean schwer = false;
    private String difficulty = "easy";
    private String operationVariant = "add";
    private PageFragment myFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        getSupportActionBar().hide(); // hide the title bar
        setContentView(R.layout.activity_main);
        initElements();
        // Get the ViewPager and set it's PagerAdapter so that it can display items
        // ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);
        //viewPager.setAdapter(new SampleFragmentPagerAdapter(getSupportFragmentManager(),
        //      MainActivity.this));

        // Give the TabLayout the ViewPager
        //TabLayout tabLayout = (TabLayout) findViewById(R.id.sliding_tabs);
        //tabLayout.setupWithViewPager(viewPager);

        schwierigkeitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                diffPopup();
            }
        });
        anleitungButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                instrucPopup();
            }
        });
        operationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                opePopup();
            }
        });

        goButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("Go geklickt"); // Hier feedback für Go Button erstellen bei Klick
                goButton.setVisibility(View.GONE);
                schwierigkeitButton.setVisibility(View.GONE);
                anleitungButton.setVisibility(View.GONE);
                operationButton.setVisibility(View.GONE);
                logo.setVisibility(View.GONE);
               /* ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);
                viewPager.setAdapter(new SampleFragmentPagerAdapter(getSupportFragmentManager(),
                MainActivity.this));*/

                myFragment = PageFragment.newInstance();

                FragmentManager fragmentManager = getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.add(android.R.id.content, myFragment);
                fragmentTransaction.commit();

            }
        });



        //folgendes funktioniert evtl noch nicht richtig, wartezeit der schatten animation bei onTouch Action Down


        goButton.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN: {
                        goButton.setImageDrawable(getResources().getDrawable(R.drawable.play2schatten));
                        break;
                    }
                    case MotionEvent.ACTION_UP: {
                        goButton.setImageDrawable(getResources().getDrawable(R.drawable.play2));;
                        break;
                    }
                }
                return false;
            }

        });
    }

    public void initElements() {
        logo = (ImageView) findViewById(R.id.start_logo);
        diffPop = new Dialog(this);
        diffPop.setContentView(R.layout.diff_popup);
        opePop = new Dialog(this);
        opePop.setContentView(R.layout.ope_popup);
        instrucPop = new Dialog(this);
        instrucPop.setContentView(R.layout.instruc_popup);
        schwierigkeitButton = (Button) findViewById(R.id.schwierigkeit);
        operationButton = (Button) findViewById(R.id.operation);
        anleitungButton = (Button) findViewById(R.id.anleitung);
        goButton = (ImageButton) findViewById(R.id.go_button);
        leichtButton = (RadioButton) diffPop.findViewById(R.id.leicht_button);
        mittelButton = (RadioButton) diffPop.findViewById(R.id.mittel_button);
        schwerButton = (RadioButton) diffPop.findViewById(R.id.schwer_button);
        addButton = (RadioButton) opePop.findViewById(R.id.add_button);
        subButton = (RadioButton) opePop.findViewById(R.id.sub_button);
        multButton = (RadioButton) opePop.findViewById(R.id.mult_button);
        okDiffButton = (Button) diffPop.findViewById(R.id.okdiff_button);
        okInstrucButton = (Button) instrucPop.findViewById(R.id.okinstruc_button);
        okOpeButton = (Button) opePop.findViewById(R.id.ok_ope);
    }

    public String getDifficulty() {
        return difficulty;
    }

    public String getOperationVariant(){
        return operationVariant;
    }

    public void diffPopup() {
        okDiffButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    if (leichtButton.isChecked()) {
                        difficulty = "easy";
                    }
                    if (mittelButton.isChecked()) {
                        difficulty = "mid";
                    }
                    if (schwerButton.isChecked()) {
                        difficulty = "hard";
                    }
                    diffPop.dismiss();

            }
        });

        diffPop.setCanceledOnTouchOutside(false);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(diffPop.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;
        diffPop.getWindow().setAttributes(lp);
        diffPop.show();
    }

    public void instrucPopup() {
        okInstrucButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                instrucPop.dismiss();
            }
        });
        instrucPop.setCanceledOnTouchOutside(false);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(instrucPop.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;
        instrucPop.getWindow().setAttributes(lp);
        instrucPop.show();
    }

    public void opePopup() {
        okOpeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    if (addButton.isChecked()) {
                        operationVariant = "add";
                    }
                    if (subButton.isChecked()) {
                        operationVariant = "sub";
                    }
                    if (multButton.isChecked()) {
                        operationVariant = "mult";
                    }
                    opePop.dismiss();
            }
        });

        opePop.setCanceledOnTouchOutside(false);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(opePop.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;
        opePop.getWindow().setAttributes(lp);
        opePop.show();
    }

}
