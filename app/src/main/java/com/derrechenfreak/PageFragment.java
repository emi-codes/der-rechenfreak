package com.example.emiram.derrechenfreak;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.CountDownTimer;
import android.os.Vibrator;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import java.util.Objects;
import java.util.Random;

/**
 * Created by Emiram on 19.04.2017.
 */

// In this case, the fragment displays simple text based on the page
public class PageFragment extends Fragment {
    private View view;
    private int counterRichtig;
    private int counterFalsch;
    private int counterInsgesamt;
    private int rndIntValue;
    private int rndIntFinale;
    private int helpIntRnd;
    private int punktzahl;
    private TextView zahl1View;
    private int random2;
    private int random1;
    private TextView zahl2View;
    private TextView ergebnisView;
    private TextView insgesamtCounterView;
    private TextView richtigCounterView;
    private TextView falschCounterView;
    private TextView richtigTextView;
    private TextView falschTextView;
    private TextView rechenZeichen;
    private TextView resultEnd;
    private TextView punkteView;
    private Random rndm;
    private int finalResult;
    private int rightResult;
    private int helpInt;
    private Vibrator vibration;
    private int i = 100;
    private CountDownTimer mCountDownTimer;
    private ColorStateList oldColors;
    private ProgressBar progressBar;
    private Dialog endPopup;
    private Button restartButton;
    private Button exitButton;
    private Button rightButton;
    private Button wrongButton;

    public static PageFragment newInstance() {
        PageFragment fragment = new PageFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        vibration = (Vibrator) getActivity().getSystemService(Context.VIBRATOR_SERVICE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_page2, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState){
        initViews(view);
        settings();
    }

    public void settings() {
        MainActivity ma = (MainActivity) getActivity();
        rndIntFinale = 6;
        helpIntRnd = 11;
        switch(ma.getDifficulty()) {
            case "easy":
                rndIntValue = 20;
                break;
            case "mid":
                rndIntValue = 100;
                break;
            case "hard":
                rndIntValue = 300;
                break;
        }

        switch (ma.getOperationVariant()) {
            case "add":
                addOperation();
                break;
            case "sub":
                minusOperation();
                break;
            case "mult":
                multOperation();
                break;
        }
    }

    public void initViews(View view){
        zahl1View = (TextView) view.findViewById(R.id.zahl1);
        rechenZeichen = (TextView) view.findViewById(R.id.rechen_zeichen);
        zahl2View = (TextView) view.findViewById(R.id.zahl2);
        ergebnisView = (TextView) view.findViewById(R.id.ergebnis);
        insgesamtCounterView = (TextView) view.findViewById(R.id.insgesamtCounter);
        richtigCounterView = (TextView) view.findViewById(R.id.richtigCounter);
        falschCounterView = (TextView) view.findViewById(R.id.falschCounter);
        richtigTextView = (TextView) view.findViewById(R.id.richtigText);
        falschTextView = (TextView) view.findViewById(R.id.falschText);
        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        progressBar.setScaleY(3f);
        rightButton = (Button) view.findViewById(R.id.richtig_button);
        wrongButton = (Button) view.findViewById(R.id.falsch_button);
        punkteView = (TextView) view.findViewById(R.id.punkte);
    }

    @SuppressLint("NewApi")
    public void endPopupView() {
        view.setVisibility(View.INVISIBLE);
        endPopup = new Dialog(getContext());
        endPopup.setContentView(R.layout.end_popup);
        resultEnd = (TextView) endPopup.findViewById(R.id.resultEnd);
        String ergebnisText = "Punkte: " + punktzahl +"\n" + "Richtige: " + counterRichtig+"\n"+"Falsche: "+counterFalsch;
        resultEnd.setText(ergebnisText);
        restartButton = (Button) endPopup.findViewById(R.id.restart_button);
        exitButton = (Button) endPopup.findViewById(R.id.exit_button);
        restartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                restart();
            }
        });

        exitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().findViewById(R.id.start_logo).setVisibility(View.VISIBLE);
                getActivity().findViewById(R.id.go_button).setVisibility(View.VISIBLE);
                getActivity().findViewById(R.id.anleitung).setVisibility(View.VISIBLE);
                getActivity().findViewById(R.id.schwierigkeit).setVisibility(View.VISIBLE);
                getActivity().findViewById(R.id.operation).setVisibility(View.VISIBLE);
                view.setVisibility(View.INVISIBLE);
                endPopup.dismiss();
                //hier muss noch alles resettet werden für ein exit
            }
        });

        endPopup.setCanceledOnTouchOutside(false);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(Objects.requireNonNull(endPopup.getWindow()).getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;

        endPopup.getWindow().setAttributes(lp);
        endPopup.show();
    }

    public void vibrateHere() {
        //Vibriere einmal in der Länge von 200 Millisekunden (0,2 Sekunden)
        vibration.vibrate(200);
    }

    public void addOperation() {
        rndm = new Random();
        rechenZeichen.setText("+");
        oldColors = punkteView.getTextColors();

        counterRichtig = 0;
        counterFalsch = 0;
        counterInsgesamt = 0;
        punktzahl = 0;
        punktzahl = counterRichtig - counterFalsch;
        punkteView.setText(String.valueOf(punktzahl));
        progressBar.setProgress(i);

        nextExerciseAdd();

        rightButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Integer.parseInt(ergebnisView.getText().toString()) == rightResult) {
                    rightAction();
                } else {
                    wrongAction();
                }
                nextExerciseAdd();
            }

        });

        wrongButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Integer.parseInt(ergebnisView.getText().toString()) != rightResult) {
                    rightAction();
                } else {
                    wrongAction();
                }
                nextExerciseAdd();
            }

        });

        mCountDownTimer = new CountDownTimer(2000200, 150) {

            @Override
            public void onTick(long millisUntilFinished) {
                Log.v("Log_tag", "Tick of Progress " + i + " " + millisUntilFinished + "ms.");
                i--;
                progressBar.setProgress(i);
                if (i <= 0) {
                    cancel();
                    onFinish();
                }
            }

            @Override
            public void onFinish() {
                System.out.println("ZEIT ABGELAUFEN!");
                endPopupView();
            }
        };
        mCountDownTimer.start();
    }

    public void nextExerciseAdd() {
        counterInsgesamt++;

        random2 = rndm.nextInt(rndIntValue);
        random1 = rndm.nextInt(rndIntValue);

        zahl1View.setText(String.valueOf(random2));
        zahl2View.setText(String.valueOf(random1));

        finalResult = rndm.nextInt(rndIntFinale);
        helpInt = rndm.nextInt(helpIntRnd);
        rightResult = random2 + random1;

        switch(finalResult){
            case 1:
                if (Integer.parseInt(String.valueOf(rightResult - helpInt)) > 0) {
                    ergebnisView.setText(String.valueOf(rightResult - helpInt));
                } else {
                    ergebnisView.setText(String.valueOf(rightResult + helpInt));
                }
                break;
            case 2:
                ergebnisView.setText(String.valueOf(rightResult + helpInt));
                break;
            case 5:
                ergebnisView.setText(String.valueOf(rightResult + helpInt));
                break;
            default:
                ergebnisView.setText(String.valueOf(rightResult));
                break;
        }

    }

    public void minusOperation() {
        rndm = new Random();
        rechenZeichen.setText("-");
        oldColors = punkteView.getTextColors();

        counterRichtig = 0;
        counterFalsch = 0;
        counterInsgesamt = 0;
        punktzahl = 0;
        punktzahl = counterRichtig - counterFalsch;
        punkteView.setText(String.valueOf(punktzahl));
        progressBar.setProgress(i);

        nextExerciseMinus();

        rightButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Integer.parseInt(ergebnisView.getText().toString()) == rightResult) {
                    rightAction();
                } else {
                    wrongAction();
                }
                nextExerciseMinus();
            }

        });

        wrongButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Integer.parseInt(ergebnisView.getText().toString()) != rightResult) {
                    rightAction();
                } else {
                    wrongAction();
                }
                nextExerciseMinus();
            }

        });

        mCountDownTimer = new CountDownTimer(2000200, 150) {

            @Override
            public void onTick(long millisUntilFinished) {
                Log.v("Log_tag", "Tick of Progress " + i + " " + millisUntilFinished + "ms.");
                i--;
                progressBar.setProgress(i);
                if (i <= 0) {
                    cancel();
                    onFinish();
                }
            }

            @Override
            public void onFinish() {
                System.out.println("ZEIT ABGELAUFEN!");
                endPopupView();
            }
        };
        mCountDownTimer.start();
    }

    public void nextExerciseMinus() {
        counterInsgesamt++;

        random2 = rndm.nextInt(rndIntValue);
        random1 = rndm.nextInt(rndIntValue);

        zahl1View.setText(String.valueOf(random2));
        zahl2View.setText(String.valueOf(random1));

        finalResult = rndm.nextInt(rndIntFinale);
        helpInt = rndm.nextInt(helpIntRnd);
        rightResult = random2 - random1;

        switch(finalResult){
            case 1:
                if (Integer.parseInt(String.valueOf(rightResult - helpInt)) > 0) {
                    ergebnisView.setText(String.valueOf(rightResult - helpInt));
                } else {
                    ergebnisView.setText(String.valueOf(rightResult + helpInt));
                }
                break;
            case 2:
                ergebnisView.setText(String.valueOf(rightResult + helpInt));
                break;
            case 5:
                ergebnisView.setText(String.valueOf(rightResult + helpInt));
                break;
            default:
                ergebnisView.setText(String.valueOf(rightResult));
                break;
        }
    }

    public void multOperation() {
        rndm = new Random();
        rechenZeichen.setText("*");
        oldColors = punkteView.getTextColors();

        counterRichtig = 0;
        counterFalsch = 0;
        counterInsgesamt = 0;
        punktzahl = 0;
        punktzahl = counterRichtig - counterFalsch;
        punkteView.setText(String.valueOf(punktzahl));
        progressBar.setProgress(i);

        nextExerciseMult();

        rightButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Integer.parseInt(ergebnisView.getText().toString()) == rightResult) {
                    rightAction();
                } else {
                    wrongAction();
                }
                nextExerciseMult();
            }

        });

        wrongButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Integer.parseInt(ergebnisView.getText().toString()) != rightResult) {
                    rightAction();
                } else {
                    wrongAction();
                }
                nextExerciseMult();
            }

        });

        mCountDownTimer = new CountDownTimer(2000200, 150) {

            @Override
            public void onTick(long millisUntilFinished) {
                Log.v("Log_tag", "Tick of Progress " + i + " " + millisUntilFinished + "ms.");
                i--;
                progressBar.setProgress(i);
                if (i <= 0) {
                    cancel();
                    onFinish();
                }
            }

            @Override
            public void onFinish() {
                System.out.println("ZEIT ABGELAUFEN!");
                endPopupView();
            }
        };
        mCountDownTimer.start();
    }

    public void nextExerciseMult() {
        counterInsgesamt++;

        random2 = rndm.nextInt(rndIntValue);
        random1 = rndm.nextInt(rndIntValue);

        zahl1View.setText(String.valueOf(random2));
        zahl2View.setText(String.valueOf(random1));

        finalResult = rndm.nextInt(rndIntFinale);
        helpInt = rndm.nextInt(helpIntRnd);
        rightResult = random2 * random1;

        switch(finalResult){
            case 1:
                if (Integer.parseInt(String.valueOf(rightResult - helpInt)) > 0) {
                    ergebnisView.setText(String.valueOf(rightResult - helpInt));
                } else {
                    ergebnisView.setText(String.valueOf(rightResult + helpInt));
                }
                break;
            case 2:
                ergebnisView.setText(String.valueOf(rightResult + helpInt));
                break;
            case 5:
                ergebnisView.setText(String.valueOf(rightResult + helpInt));
                break;
            default:
                ergebnisView.setText(String.valueOf(rightResult));
                break;
        }
    }

    public void wrongAction() {
        i -= 10;
        counterFalsch++;
        punktzahl = punktzahl - 2;

        if(punktzahl < 0){
            punktzahl = 0;
        }

        punkteView.setText(String.valueOf(punktzahl));
        punkteView.setTextColor(Color.RED);
        vibrateHere();
    }

    public void rightAction() {
        if (i<=90) i += 10;
        counterRichtig++;
        punktzahl++;
        punkteView.setText(String.valueOf(punktzahl));
        punkteView.setTextColor(Color.GREEN);
    }

    public void restart() {
        i = 100;
        settings();
        endPopup.dismiss();
        mCountDownTimer.start();
        // counterRichtig = 0;
        // counterFalsch = 0;
        // counterInsgesamt = -1;
        // punktzahl = 0;
        // punktzahl = counterRichtig - counterFalsch;
        punkteView.setText(String.valueOf(punktzahl));
        punkteView.setTextColor(oldColors);
        // nextExerciseAdd();
        view.setVisibility(View.VISIBLE);
    }
}
